#ifndef BRACCIOKINEMATICS_H
#define BRACCIOKINEMATICS_H

#include "Arduino.h"

//#define BRACCIOKINEMATICS_DEBUG

extern int pose[5];
extern int angles[5];

enum poseID {X, Y, Z, R, P};

bool reachable (int[]);
bool anglesFromPose (int[], int[]);
void poseFromAngles (int[], int[]);

#endif

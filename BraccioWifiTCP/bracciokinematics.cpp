#include "Arduino.h"
#include <math.h>
#include "bracciokinematics.h"

double joints[4] = {70, 125, 125, 190};

bool reachable(int pose[]) {

  bool reachable = sqrt(sq(double(pose[X])) + sq(double(pose[Y]))) <= joints[1] + joints[2] + joints[3];

  if (reachable) {
    if (pose[X]!=0) angles[0] = round(atan2(pose[Y], pose[X])/PI*180);
    else angles[0] = pose[Y]>0?90:-90;

    #ifdef BRACCIOKINEMATICS_DEBUG
      Serial.println(angles[0]);
    #endif

  }
  else return reachable;

  float a2  = (sq(sqrt(sq(double(pose[X])) + sq(double(pose[Y])))-joints[3]*cos(pose[P]*PI/180)) + sq(pose[Z]-joints[0]-joints[3]*sin(pose[P]*PI/180)) - sq(joints[1]) - sq(joints[2]) ) / (2*joints[1]*joints[2]);
  reachable = reachable && (a2>=float(-0.01)) && (a2<=float(1));
  if (reachable) {
    angles[2] = - acos(a2)/PI*180;

    #ifdef BRACCIOKINEMATICS_DEBUG
      Serial.println(angles[2]);
    #endif

  }
  else return reachable;

  float a1 = ( ( (pose[Z]-joints[0]-joints[3]*sin(pose[P]*PI/180)) * (joints[1]+joints[2]*cos(angles[2]*PI/180)) ) - joints[2]*sin(angles[2]*PI/180)*(sqrt(sq(double(pose[X])) + sq(double(pose[Y]))) - joints[3]*cos(pose[P]*PI/180)) ) / (sq(joints[1])+sq(joints[2])+2*joints[1]*joints[2]*cos(angles[2]*PI/180) );
  reachable = reachable && (abs(a1)<=1);
  if (reachable) {
    if (abs(a1)<=0.999) angles[1] = round(asin(a1)/PI*180);
    else angles[1] = 90;

    #ifdef BRACCIOKINEMATICS_DEBUG
      Serial.println(angles[1]);
    #endif

  }
  reachable = reachable && (angles[1])>=0;

  return reachable;
  
};

bool anglesFromPose(int pose[], int angles[]) {

  if (reachable(pose)) {

    #ifdef BRACCIOKINEMATICS_DEBUG
      Serial.println("reachable");
      Serial.println(angles[0]);
      Serial.println(angles[1]);
      Serial.println(angles[2]);
    #endif

    if ((sq(double(pose[X]))+sq(double(pose[Y]))<sq(double(pose[Z] + joints[0]))) && (pose[Z]>0)) { 
      angles[1] = 180 - angles[1];
    }
    angles[2] += 90;
    angles[3] = pose[P] - angles[1] - (angles[2]-90) + 90;
    angles[4] = pose[R];

    if      ((angles[0]> 0) && (pose[Y]< 0)) angles[0] = 180 - angles[0];
    else if ((angles[0]<=0) && (pose[Y]<=0)) angles[0] = -angles[0];
    else {
      angles[0] = 180 - angles[0];
      angles[1] = 180 - angles[1];
      angles[2] = 180 - angles[2];
      angles[3] = 180 - angles[3];
      angles[4] = 180 - angles[4];
    }

    #ifdef BRACCIOKINEMATICS_DEBUG
      Serial.println("fine IK");
      Serial.println(angles[0]);
      Serial.println(angles[1]);
      Serial.println(angles[2]);
      Serial.println(angles[3]);
      Serial.println(angles[4]);
    #endif

    return angles[3]>=0;
  } else return false;

}

void poseFromAngles(int angles[], int pose[]) {

  pose[X] = (joints[1]*cos(angles[1]*PI/180) + joints[2]*cos(angles[1]*PI/180 + (angles[2]-90)*PI/180) + joints[3]*cos(angles[1]*PI/180 + (angles[2]-90)*PI/180 + (angles[3]-90)*PI/180))*cos(2*PI - angles[0]*PI/180);
  pose[Y] = (joints[1]*cos(angles[1]*PI/180) + joints[2]*cos(angles[1]*PI/180 + (angles[2]-90)*PI/180) + joints[3]*cos(angles[1]*PI/180 + (angles[2]-90)*PI/180 + (angles[3]-90)*PI/180))*sin(2*PI - angles[0]*PI/180);
  pose[Z] = joints[0] + joints[1]*sin(angles[1]*PI/180) + joints[2]*sin(angles[1]*PI/180 + (angles[2]-90)*PI/180) + joints[3]*sin(angles[1]*PI/180 + (angles[2]-90)*PI/180 + (angles[3]-90)*PI/180);
  pose[P] = angles[1] + (angles[2]-90) + (angles[3]-90);
  pose[R] = angles[4];

  #ifdef BRACCIOKINEMATICS_DEBUG
    Serial.println("fine DK");
    Serial.println(pose[X]);
    Serial.println(pose[Y]);
    Serial.println(pose[Z]);
    Serial.println(pose[R]);
    Serial.println(pose[P]);
  #endif

}
/*
  BraccioWiFi TCP

  created 06 Nov 2020
  by Vincenzo Calligaro
 */

//#define MAIN_DEBUG

#include <SPI.h>
#include <WiFiNINA.h>
#include "arduino_secrets.h"
///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = SECRET_SSID;        // your network SSID (name)
char pass[] = SECRET_PASS;        // your network password (use for WPA, or use as key for WEP)
char host[] = HOSTNAME;           // the hostname of the board
int keyIndex = 0;                 // your network key index number (needed only for WEP)

int status = WL_IDLE_STATUS;
WiFiServer server(80);
unsigned long idleTime = 0;

String response;

#include <Servo.h>
#include <Braccio.h>

//Initial Value for each Motor
int step = 30;
int m1 = 0;
int m2 = 135;
int m3 = 0;
int m4 = 0;
int m5 = 90;
int m6 = 73;

Servo base;
Servo shoulder;
Servo elbow;
Servo wrist_rot;
Servo wrist_ver;
Servo gripper;

int led =  LED_BUILTIN;

#include "bracciokinematics.h"

int angles[] = {m1, m2, m3, m4, m5};
int pose[] = {134, 0, 112, 90, -45};

//#include <Arduino_LSM6DS3.h>
//
//float IMUx, IMUy, IMUz, IMUr, IMUp, IMUj;

void setup() {

  #ifdef MAIN_DEBUG
    //Initialize serial and wait for port to open:
    Serial.begin(9600);
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  #endif

  pinMode(led, OUTPUT);      // set the LED pin mode

  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    #ifdef MAIN_DEBUG
      Serial.println("Communication with WiFi module failed!");
      // don't continue
    #endif
    while (true);
  }

  #ifdef MAIN_DEBUG
    String fv = WiFi.firmwareVersion();
    if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
      Serial.println("Please upgrade the firmware");
    }
  #endif

  #ifdef MAIN_DEBUG
    Serial.print("Setting HostName to ");
    Serial.println(host);
  #endif
  WiFi.setHostname(host);
  delay(5000);
  #ifdef MAIN_DEBUG
    Serial.print("HostName set to ");
    Serial.println(host);
  #endif

  // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    #ifdef MAIN_DEBUG
      Serial.print("Attempting to connect to Network named: ");
      Serial.println(ssid);                   // print the network name (SSID);
    #endif

    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
  }
  server.begin();                           // start the web server on port 80
  #ifdef MAIN_DEBUG
    printWifiStatus();                        // you're connected now, so print out the status
  #endif

  //Intitialization of Braccio
  Braccio.begin();
  Braccio.ServoMovement(30, 0, 135, 0, 0, 90, 73);

  //#ifdef MAIN_DEBUG
  //  Serial.println("Initializing IMU");
  //#endif
  //  if (!IMU.begin()) {
  //#ifdef MAIN_DEBUG
  //    Serial.println("Failed Initializing IMU!");
  //#endif
  //    while(true) {}
  //  }
  //  else {
  //#ifdef MAIN_DEBUG
  //    Serial.println("IMU Initialized");
  //#endif  
  //  }

}

void loop() {
  WiFiClient client = server.available();   // listen for incoming clients

  if (client) {                             // if you get a client,
    #ifdef MAIN_DEBUG
      Serial.println("new client");           // print a message out the serial port
    #endif
    String currentLine = "";                // make a String to hold incoming data from the client
    if (client.connected()) {            // loop while the client's connected
      while (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        #ifdef MAIN_DEBUG
          Serial.write(c);                    // print it out the serial monitor
        #endif
        currentLine += c;                   // add it to the end of the currentLine
      }
    }

    currentLine.trim();
    #ifdef MAIN_DEBUG
      Serial.println();
      Serial.print("Request: ");
      Serial.println(currentLine);
    #endif

    if (currentLine.startsWith("pos/")) {
      step    = parseCommand(currentLine, "pMOVESTP");
      pose[X] = parseCommand(currentLine, "pX");
      pose[Y] = parseCommand(currentLine, "pY");
      pose[Z] = parseCommand(currentLine, "pZ");
      pose[P] = parseCommand(currentLine, "pP");
      pose[R] = parseCommand(currentLine, "pR");
      m6      = parseCommand(currentLine, "pMOVEM6");

      #ifdef BRACCIOKINEMATICS_DEBUG
        Serial.println(step);
        Serial.println(pose[X]);
        Serial.println(pose[Y]);
        Serial.println(pose[Z]);
        Serial.println(pose[R]);
        Serial.println(pose[P]);
        Serial.println("Primi angoli calcolati");
      #endif

      if (anglesFromPose(pose, angles)) {
        response = "motstp=" + String(step) + "&m1=" + String(angles[0]) + "&m2=" + String(angles[1]) + "&m3=" + String(angles[2]) + "&m4=" + String(angles[3]) + "&m5=" + String(angles[4]) + "&m6=" + String(m6);
      } else {
        response = "unreachable";
      }

    } else if (currentLine.startsWith("mot/")) {
      step    = parseCommand(currentLine, "pMOTSTP");
      angles[0] = parseCommand(currentLine, "pM1");
      angles[1] = parseCommand(currentLine, "pM2");
      angles[2] = parseCommand(currentLine, "pM3");
      angles[3] = parseCommand(currentLine, "pM4");
      angles[4] = parseCommand(currentLine, "pM5");
      m6      = parseCommand(currentLine, "pM6");

      poseFromAngles(angles, pose);
      response = "movestp=" + String(step) + "&x=" + String(pose[X]) + "&y=" + String(pose[Y]) + "&z=" + String(pose[Z]) + "&p=" + String(pose[P]) + "&r=" + String(pose[R]) + "&movem6=" + String(m6);

    } else if (currentLine.startsWith("getActualPose")) response = "movestp=" + String(step) + "&x=" + String(pose[X]) + "&y=" + String(pose[Y]) + "&z=" + String(pose[Z]) + "&p=" + String(pose[P]) + "&r=" + String(pose[R]) + "&movem6=" + String(m6);
    else if (currentLine.startsWith("getActualMot")) response = "motstp=" + String(step) + "&m1=" + String(angles[0]) + "&m2=" + String(angles[1]) + "&m3=" + String(angles[2]) + "&m4=" + String(angles[3]) + "&m5=" + String(angles[4]) + "&m6=" + String(m6);
    else {      
      if (response!="unreachable") Braccio.ServoMovement(step, angles[0], angles[1], angles[2], angles[3], angles[4], m6);
      int sent = client.print(response);

      #ifdef MAIN_DEBUG
        Serial.println(sent);
      #endif

      // close the connection:
      client.stop();
      #ifdef MAIN_DEBUG
        Serial.println("client disconnected");
      #endif

      #ifdef BRACCIOKINEMATICS_DEBUG
        Serial.println("Angoli finali");
        Serial.println(step);
        Serial.println(angles[0]);
        Serial.println(angles[1]);
        Serial.println(angles[2]);
        Serial.println(angles[3]);
        Serial.println(angles[4]);
        Serial.println(m6);
      #endif

    }

    #ifdef MAIN_DEBUG
      Serial.print("Response: ");
      Serial.println(response);
    #endif

    //    idleTime=millis();

  }
  //  else {
  //    if (millis()>idleTime+5000) {
  //      idleTime=millis();
  //      Braccio.ServoMovement(30, 0, 135, 0, 0, 90, 73);
  //    }
  //  }

  //  if (IMU.accelerationAvailable()) {
  //        IMU.readAcceleration(IMUx, IMUy, IMUz);
  //
  //#ifdef MAIN_DEBUG
  //        Serial.print(IMUx);
  //        Serial.print('\t');
  //        Serial.print(IMUy);
  //        Serial.print('\t');
  //        Serial.println(IMUz);
  //#endif
  //  }
  //
  //  if (IMU.gyroscopeAvailable()) {
  //        IMU.readGyroscope(IMUr, IMUp, IMUj);
  //
  //#ifdef MAIN_DEBUG
  //        Serial.print(IMUr);
  //        Serial.print('\t');
  //        Serial.print(IMUp);
  //        Serial.print('\t');
  //        Serial.println(IMUj);
  //#endif
  //  }

  //#ifdef MAIN_DEBUG
  //  Serial.print ("Tempo ciclo: ");
  //  Serial.println(millis());
  //#endif

}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  // print where to go in a browser:
  Serial.print("To see this page in action, open a browser to http://");
  Serial.print(ip);
  Serial.print(" or http://");
  Serial.println(host);
}

/**
Parse Command from TCP
It parse a command like: STEP=20&BASE=0&SHOULDER=135&ELBOW=0&WRISTV=0&WRISTR=90&GRIPPER=0
@param command: The message to parse
@param type: the key for parsing
@return the value for the key 
*/
int parseCommand(String command, String type) {
  int typeIndex = command.indexOf(type);
  int dotsIndex = command.indexOf('=', typeIndex + type.length());

  int idxtmp = 1;
  while (isDigit(command[dotsIndex+idxtmp]) || (command[dotsIndex+idxtmp]=='-')) idxtmp++;

  String tmp = command.substring(dotsIndex + 1, dotsIndex + idxtmp);
  return tmp.toInt();
}

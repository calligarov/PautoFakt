let busy = false;
let seqRunning = false;
let seqInLoop = false;

async function getActualVals() {
    busy = !busy;

    for (let lbl of document.querySelectorAll(".movelbl")) document.getElementById(lbl.id).style.color="#FF9900";

    await fetch("/arduino/getActualPose", {
                method: "POST",
    })
    .then( (response) => {
        response.text().then( (text) => {
            res = new URLSearchParams(text);

            for (let [resid, resval] of res) {
                document.getElementById(resid).value = resval.trim();
                document.getElementById(resid + "val").innerHTML = resval.trim();
            }
        });
    });

    await fetch("/arduino/getActualMot", {
                method: "POST",
    })
    .then( (response) => {
        response.text().then( (text) => {
            res = new URLSearchParams(text);

            for (let [resid, resval] of res) {
                document.getElementById(resid).value = resval.trim();
                document.getElementById(resid + "val").innerHTML = resval.trim();
            }
        });
    });
    busy = !busy;

    for (let lbl of document.querySelectorAll(".movelbl")) document.getElementById(lbl.id).style.color="green";

}

async function getStoredSeq() {
    
    await fetch("/arduino/getStoredSeq", {
                method: "POST",
    })
    .then( (response) => {
        response.text().then( (text) => {
            let tbl = document.createElement("TABLE");
        
            document.getElementById("seqDiv").insertBefore(tbl, document.getElementById("seqDiv").childNodes[0]);
            tbl.setAttribute("id", "seq");

            while (text!="") {
                
                let tblCell, tblBtn;

                if (text!="\r\n") {
                    tbl.insertRow(-1);
            
                    while (!text.startsWith("\r\n")) {
                        tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

                        tblCell.innerHTML = text.substring(0, text.indexOf(";"));
                        text = text.substring(tblCell.innerHTML.length+1);
                    }

                    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

                    tblBtn = document.createElement("BUTTON");
                    tblBtn.innerHTML = "Edit";
                    tblBtn.setAttribute("onclick", "editReq(" + String(tbl.rows.length-1) + ")");
                    tblCell.appendChild(tblBtn);

                    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

                    tblBtn = document.createElement("BUTTON");
                    tblBtn.innerHTML = "Delete";
                    tblBtn.setAttribute("onclick", "deleteReq(" + String(tbl.rows.length-1) + ")");
                    tblCell.appendChild(tblBtn);

                    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

                    tblBtn = document.createElement("BUTTON");
                    tblBtn.innerHTML = "Move Up";
                    tblBtn.setAttribute("onclick", "moveReqUp(" + String(tbl.rows.length-1) + ")");
                    tblCell.appendChild(tblBtn);

                    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

                    tblBtn = document.createElement("BUTTON");
                    tblBtn.innerHTML = "Move Down";
                    tblBtn.setAttribute("onclick", "moveReqDown(" + String(tbl.rows.length-1) + ")");
                    tblCell.appendChild(tblBtn);

                }

                text = text.substring(2);

            }
        });
    });
}

function getPos() {
    let pos = new URLSearchParams();
    for (let val of document.getElementById("move")) pos.append("p" + val.id.toUpperCase(), val.value);
    return pos;
}

function getMot() {
    let mot = new URLSearchParams();
    for (let val of document.getElementById("mot")) mot.append("p" + val.id.toUpperCase(), val.value);
    return mot;
}

async function sendReq(type) {
    let reachcol;

    for (let lbl of document.querySelectorAll(".movelbl")) document.getElementById(lbl.id).style.color="#FF9900";
    
    if (type=="pos")	for (let mov of document.getElementById("move")) document.getElementById(mov.id + "val").innerHTML = mov.value;
    else if (type=="mot") for (let mot of document.getElementById("mot")) document.getElementById(mot.id + "val").innerHTML = mot.value;

    if (!busy) {
        busy = !busy;
        let req = "/arduino/" + type +"/";
        let actualReq = "";

        if (type=="pos") req = req + getPos();
        else if (type=="mot") req = req + getMot();

        while (actualReq!=req) {

            await fetch(req, {
                        method: "POST",
            })
            .then( (response) => {
                response.text().then( (text) => {
                    if (text.trim() == "unreachable") reachcol="red";
                    else {
                        reachcol="green";
                    
                        if (!seqRunning) {
                            res = new URLSearchParams(text);

                            for (let [resid, resval] of res) {
                                document.getElementById(resid).value = resval.trim();
                                document.getElementById(resid + "val").innerHTML = resval.trim();
                            }
                        }
                    }

                    for (let lbl of document.querySelectorAll(".movelbl")) document.getElementById(lbl.id).style.color=reachcol;

                });
            });

            actualReq = req;

            req = "/arduino/" + type +"/";

            if (type=="pos") req = req + getPos();
            else if (type=="mot") req = req + getMot();

        }

        busy=!busy;

    }
}

function showSection(sectionToShow) {
    for (let elm of document.getElementsByTagName("div")){
        for (let frm of elm.children) {
            if ((frm.tagName == "FORM") || (frm.tagName == "TABLE")) {
                if (frm.id == sectionToShow) {
                    elm.style.display = "block";
                    document.getElementById(frm.id + "Btn").style.backgroundColor = "#FF9900";
                }
                else {
                    elm.style.display = "none";
                    document.getElementById(frm.id + "Btn").style.backgroundColor = "#FFE0B2";
                }
            }
        }
    }
}

async function addToSeq(type) {
    let tbl = document.getElementById("seq");
    let tblLen, tblCell, reqToStore, tblBtn;

    tbl.insertRow(-1);
    tblLen = tbl.rows.length;

    tblCell = tbl.rows[tblLen-1].insertCell(0);
    tblCell.innerHTML = type;

    switch (type) {
        case "pos":
            for (let val of document.getElementById("move")) {
                tblCell = tbl.rows[tblLen-1].insertCell(-1);
                tblCell.innerHTML = val.value;
            }

            break;

        case "mot":
            for (let val of document.getElementById("mot")) {
                tblCell = tbl.rows[tblLen-1].insertCell(-1);
                tblCell.innerHTML = val.value;
            }

            break;
    
        default:
            break;
    }

    reqToStore = "/arduino/storeReqToSD/";

    for (let val of tbl.rows[tblLen-1].cells) reqToStore = reqToStore + val.innerHTML + ";";

    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

    tblBtn = document.createElement("BUTTON");
    tblBtn.innerHTML = "Edit";
    tblBtn.setAttribute("onclick", "editReq(" + String(tbl.rows.length-1) + ")");
    tblCell.appendChild(tblBtn);

    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

    tblBtn = document.createElement("BUTTON");
    tblBtn.innerHTML = "Delete";
    tblBtn.setAttribute("onclick", "deleteReq(" + String(tbl.rows.length-1) + ")");
    tblCell.appendChild(tblBtn);

    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

    tblBtn = document.createElement("BUTTON");
    tblBtn.innerHTML = "Move Up";
    tblBtn.setAttribute("onclick", "moveReqUp(" + String(tbl.rows.length-1) + ")");
    tblCell.appendChild(tblBtn);

    tblCell = tbl.rows[tbl.rows.length-1].insertCell(-1);

    tblBtn = document.createElement("BUTTON");
    tblBtn.innerHTML = "Move Down";
    tblBtn.setAttribute("onclick", "moveReqDown(" + String(tbl.rows.length-1) + ")");
    tblCell.appendChild(tblBtn);

    await fetch(reqToStore, {
                method: "POST",
    });

}

async function editReq(reqNbr) {
    let valIdx = 1;
    let tbl = document.getElementById("seq");

    type = tbl.rows[reqNbr].cells[0].innerHTML;

    switch (type) {
        case "pos":
            for (let val of document.getElementById("move")) {
                tbl.rows[reqNbr].cells[valIdx].innerHTML = val.value;
                valIdx = valIdx +1;
            }

            break;

        case "mot":
            for (let val of document.getElementById("mot")) {
                tbl.rows[reqNbr].cells[valIdx].innerHTML = val.value;
                valIdx = valIdx +1;
            }

            break;
    
        default:
            break;
    }
    
    await updateSeq();

}

async function deleteReq(reqNbr) {
    
    document.getElementById("seq").deleteRow(reqNbr);
    await updateSeq();

}

async function moveReqUp(reqNbr) {
    let tbl = document.getElementById("seq");
    let rowTmp = tbl.rows[reqNbr];

    tbl.deleteRow(reqNbr);

    if (reqNbr==0) {
        tbl.insertRow(-1);
        tbl.rows[tbl.rows.length-1].innerHTML = rowTmp.innerHTML;
    } else {
        tbl.insertRow(reqNbr-1);
        tbl.rows[reqNbr-1].innerHTML = rowTmp.innerHTML;
    }

    await updateSeq();

}

async function moveReqDown(reqNbr) {
    let tbl = document.getElementById("seq");
    let rowTmp = tbl.rows[reqNbr];

    tbl.deleteRow(reqNbr);

    if (reqNbr==(tbl.rows.length)) {
        tbl.insertRow(0);
        tbl.rows[0].innerHTML = rowTmp.innerHTML;
    } else {
        tbl.insertRow(reqNbr+1);
        tbl.rows[reqNbr+1].innerHTML = rowTmp.innerHTML;
    }

    await updateSeq();

}

async function updateSeq() {

    for (let seqBtn of document.getElementById("seq").getElementsByTagName("BUTTON")) seqBtn.disabled=true;

    await fetch("/arduino/deleteSeq", {
                method: "POST",
    });

    for (let r of document.getElementById("seq").rows) {
        let reqToStore = "/arduino/storeReqToSD/";

        for (let c of r.cells) {
            if (c.children.length!=0) {
                if (c.children[0].innerText=="Edit") c.children[0].setAttribute("onclick", "editReq(" + String(r.rowIndex) + ")");
                else if (c.children[0].innerText=="Delete") c.children[0].setAttribute("onclick", "deleteReq(" + String(r.rowIndex) + ")");
                else if (c.children[0].innerText=="Move Up") c.children[0].setAttribute("onclick", "moveReqUp(" + String(r.rowIndex) + ")");
                else if (c.children[0].innerText=="Move Down") c.children[0].setAttribute("onclick", "moveReqDown(" + String(r.rowIndex) + ")");
            } else {
                reqToStore = reqToStore + c.innerHTML + ";";
            }
        }

        await fetch(reqToStore, {
            method: "POST",
        });
    }

    for (let seqBtn of document.getElementById("seq").getElementsByTagName("BUTTON")) seqBtn.disabled=false;
    
}

async function runSeq() {

    if (!seqRunning) {

        seqRunning = !seqRunning;
    
        do await fetch("/arduino/runSeq", {
                        method: "POST",
                });
        while (seqInLoop);
    
        seqRunning = !seqRunning;

    }

}

function toggleLoop() {
    seqInLoop = !seqInLoop;
    
    if (seqInLoop) document.getElementById("toggleLoopBtn").innerHTML = "Run in loop";
    else document.getElementById("toggleLoopBtn").innerHTML = "Run Once";
    
}
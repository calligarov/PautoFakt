#include <Bridge.h>
#include <Console.h>
#include <BridgeServer.h>

#include "Comm.h"
BridgeServer server;
char PArm[] = "PArm";

#include <FileIO.h>
#define STORESEQFILE "/www/sd/YunTerface/seq.csv"

//#define MAIN_DEBUG

void setup() {

  #ifdef MAIN_DEBUG
    //Initialize serial and wait for port to open:
    SerialUSB.begin(9600);
    while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
    }
  #endif
  
  #ifdef MAIN_DEBUG
    SerialUSB.println("Starting Bridge");
  #endif
    Bridge.begin();
  #ifdef MAIN_DEBUG
    SerialUSB.println("Bridge Started");
  #endif

  server.listenOnLocalhost();
  #ifdef MAIN_DEBUG
    SerialUSB.println("Starting Server");
  #endif
    server.begin();
  #ifdef MAIN_DEBUG
    SerialUSB.println("Server Started");
  #endif

  #ifdef MAIN_DEBUG
    SerialUSB.println("Starting FileSystem");
  #endif
    FileSystem.begin();
  #ifdef MAIN_DEBUG
    SerialUSB.println("FileSystem Started");
  #endif

}

void loop() {

  BridgeClient client = server.accept();

  // There is a new client?
  if (client) {
    #ifdef MAIN_DEBUG
        SerialUSB.println("New client");
    #endif
    
    // read the command
    String req = getMsg(client);
    req.trim();
    

    #ifdef MAIN_DEBUG
        SerialUSB.println();
        SerialUSB.print("Request: ");
        SerialUSB.println(req);
    #endif

    if (req.startsWith("storeReqToSD/")) {

      #ifdef MAIN_DEBUG
            SerialUSB.println();
            SerialUSB.print("Req to save: ");
            SerialUSB.println(req.substring(13));
      #endif 

      File seqFile = FileSystem.open(STORESEQFILE, FILE_APPEND);

      if (seqFile) {
        
        seqFile.println(req.substring(13));
        seqFile.close();

        sendMsg(client, "Request Saved");
        
      } else sendMsg(client, "Unable to open file");
      
    } else if (req.startsWith("deleteSeq")) {

      #ifdef MAIN_DEBUG
            SerialUSB.println();
            SerialUSB.print("Deleting Sequence");
      #endif 

      if (FileSystem.remove(STORESEQFILE)) sendMsg(client, "Sequence deleted");
      else sendMsg(client, "Unable to delete file");
      
    } else if (req.startsWith("getStoredSeq")) {

      #ifdef MAIN_DEBUG
            SerialUSB.println();
            SerialUSB.println("Loading saved Reqs");
      #endif

      File seqFile = FileSystem.open(STORESEQFILE, FILE_READ);

      if (seqFile) {

        String res = "";
        
        while (seqFile.available()) res += char(seqFile.read());

        sendMsg(client, res);
        
      } else sendMsg(client, "Unable to open file");

    } else if (req.startsWith("runSeq")) {

      BridgeClient PArmClient;
      File seqFile = FileSystem.open(STORESEQFILE, FILE_READ);

      #ifdef MAIN_DEBUG
            SerialUSB.println();
            SerialUSB.println("Running Sequence");
      #endif

      if (seqFile) {

        String res = "";

        while (seqFile.available()) {

          bool isPos=false;
          bool isMot=false;

          req = "";

          while ((req.indexOf('\r')==-1) && (req.indexOf('\n')==-1)) {

            char c = seqFile.read();

            while (c!=';') {

              req += c;

              if (req=="pos") {
                isPos=true;
                req += '/';
              } else if (req=="mot") {
                isMot=true;
                req += '/';
              }

              c = seqFile.read();
              
            }

            if (isPos) {

              if (req.indexOf("pMOVESTP")==-1) req += "pMOVESTP=";
              else if (req.indexOf("pX")==-1) req += "&pX=";
              else if (req.indexOf("pY")==-1) req += "&pY=";
              else if (req.indexOf("pZ")==-1) req += "&pZ=";
              else if (req.indexOf("pP")==-1) req += "&pP=";
              else if (req.indexOf("pR")==-1) req += "&pR=";
              else if (req.indexOf("pMOVEM6")==-1) req += "&pMOVEM6=";
              else {
                c = seqFile.read();
                req += c;
                c = seqFile.read();
                req += c;
              }
              
            } else if (isMot) {

              if (req.indexOf("pMOTSTP")==-1) req += "pMOTSTP=";
              else if (req.indexOf("pM1")==-1) req += "&pM1=";
              else if (req.indexOf("pM2")==-1) req += "&pM2=";
              else if (req.indexOf("pM3")==-1) req += "&pM3=";
              else if (req.indexOf("pM4")==-1) req += "&pM4=";
              else if (req.indexOf("pM5")==-1) req += "&pM5=";
              else if (req.indexOf("pM6")==-1) req += "&pM6=";
              else {
                c = seqFile.read();
                req += c;
                c = seqFile.read();
                req += c;
              }

            }
          }

          req = req.substring(0, req.length()-2);

          if (!PArmClient.connected())  PArmClient= contactArm(PArm);
      
          do sendMsg(PArmClient, req);
          while (!PArmClient.available());
      
          PArmClient.stop();

        }

      } else sendMsg(client, "Unable to open file");
      
    } else if (req.startsWith("pos/") || req.startsWith("mot/") || req.startsWith("getActualPose") || req.startsWith("getActualMot")) {

      BridgeClient PArmClient;
  
      if (!PArmClient.connected())  PArmClient= contactArm(PArm);
  
      do sendMsg(PArmClient, req);
      while (!PArmClient.available());
      
      String res = getMsg(PArmClient);
      sendMsg(client, res);
  
      #ifdef MAIN_DEBUG
            SerialUSB.println();
            SerialUSB.print("Response: ");
            SerialUSB.println(res);
      #endif
  
      PArmClient.stop();

    } else sendMsg(client, "Unknown request");
  
    // Close connection and free resources.
    #ifdef MAIN_DEBUG
        SerialUSB.println();
        SerialUSB.println("Closing connections");
    #endif
  
    client.stop();
    
  }

}

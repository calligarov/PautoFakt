#ifndef COMM_H
#define COMM_H

#include "Arduino.h"
#include <BridgeClient.h>

//#define COMM_DEBUG

String getMsg(BridgeClient);
BridgeClient contactArm(char[]);
void sendMsg(BridgeClient, String);

#endif

#include "Arduino.h"
#include "Comm.h"

String getMsg(BridgeClient client) {

  String msg = "";

  if (client.connected()) {
    while (client.available()) msg += char(client.read());

  #ifdef COMM_DEBUG
      SerialUSB.print("Received: ");
      SerialUSB.println(msg);
      SerialUSB.println("EOT");
  #endif

  }

  return msg;
  
}



BridgeClient contactArm(char URL[]) {

  BridgeClient Arm;

  #ifdef COMM_DEBUG
    SerialUSB.print("Connecting to ");
    SerialUSB.println(URL);
  #endif

  Arm.connect(URL, 80);

  while (!Arm.connected()) {
    delay(3000);
    #ifdef COMM_DEBUG
        SerialUSB.print(".");
        Arm.connect(URL, 80);
    #endif
  }
  
  #ifdef COMM_DEBUG
    SerialUSB.print("Connected to ");
    SerialUSB.println(URL);
  #endif

  return Arm;

}



void sendMsg(BridgeClient client, String msg) {

  #ifdef COMM_DEBUG
    SerialUSB.print("Sending ");
    SerialUSB.println(msg);
  #endif

  int sent = client.println(msg);

  #ifdef COMM_DEBUG
    SerialUSB.println(sent);
    SerialUSB.println(msg);
  #endif

  delay(500);

}
